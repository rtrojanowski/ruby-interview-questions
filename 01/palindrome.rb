class PalindromeSeparator
  FILENAME = 'words.txt'

  def initialize()
    @palindromes = []
    @not_palindromes = []
  end

  def separate
    File.foreach(FILENAME).each do |line|
       line = line.chomp
       is_palindrome?(line) ? add_palindrome(line) : add_not_palindrome(line)
    end
  end

  def output
    puts "Palindromes:"
    puts "____________"
    @palindromes.each { |s| puts s }
    puts ""
    puts "Not Palindromes:"
    puts "________________"
    @not_palindromes.each { |s| puts s }
  end

  private

  def add_palindrome(string)
    @palindromes << string
  end

  def add_not_palindrome(string)
    @not_palindromes << string
  end

  def is_palindrome?(string)
    string = string.gsub(/[^a-z]/i, '').gsub(/[[:space:]]/, '')
    string.downcase == string.downcase.reverse
  end
end

palindrome_separator = PalindromeSeparator.new
palindrome_separator.separate
palindrome_separator.output
