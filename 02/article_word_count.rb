words_count = {}

File.foreach('article.txt').each do |line|
   line = line.gsub(/[^a-z0-9]/i, ' ')

   words = line.split(' ').each do |word|
     word = word.downcase

     if words_count[word].nil?
       words_count[word] = 1
     else
       words_count[word] += 1
     end
   end
end

words_count.sort.to_h.each do |word, count|
  puts "#{word} occured #{count} time(s)."
end
